# README #

.Net Core e-Commerce Web API

### What is this repository for? ###

* The is an e-commerce backend API. To provide frontend apps with database interaction and processing.
* Version 1.0

### Technology Stack & Features ###
1.  ASP.Net Core
2.  C#
3.  Visual Studio Code
4.  Microsoft SQL Server
5.	.Net 5.0
6.	xUnit Tests
6.	SOLID Principles
7.  Asynchronous Tasks
8.  Dependancy Injection
9.  Generic Repository
10. Unit of Work
11. Entity Framework Core
12.	Swagger UI Documentation


### How do I get set up? ###

#### The solution consists of the following projects ####

1.	App.API
2.	App.Domain
3.	App.Model
4.	App.Repository
5.	App.Test


#### Configuration ####

1.	Copy the clone link from this repo.
2.	Open Visual Studio Code or Visual Studio Professionl/Enterprise
3.	Click on clone repository and past the link. Alternatively, you may open Powershell and navigate to your projects folder. Then type `git clone CloneUrl` or right click, and your command pallete will automatically paste the link for you.

#### Dependencies

Visual Studio Code/Professional/Enterprise will automatically import dependecies required by the solution.

#### Database configuration

1.	Open Microsoft SQL Server
2.	Create a database called e-commerce
3.	Execute the e-commerce-database-script.sql script located in the root folder, in your SQL Server database
4.	Update the connection string settings with your SQL Server database username & password. Connection strings are in the following files, 'App.API/appSettings.json' and 'App.Test/integrationsettings.json'

### API Endpoints

#### BundleController.cs


`http://localhost:4000/api/bundle/new`  - create new bundle
<pre>
{
	ProductId: 1,
	BundleCode: "BC1",
	DiscountPercentage: 10
}
</pre>

`http://localhost:4000/api/bundle/list-by-code/{code}` - returns single bundle item by code

`http://localhost:4000/api/bundle/list` - returns a list of bundle items

`http://localhost:4000/api/bundle/list-by-productId/{productId}` - returns a list of bundle items by product Id

#### CategoryController.cs

`http://localhost:4000/api/category/new` - creates a new category

<pre>
{
	Code: "ABC321",
	Name: "Television Sets",
}
</pre>

`http://localhost:4000/api/category/{id}` - returns a single category by id

`http://localhost:4000/api/category/edit/{id}` - updates a single category

<pre>
{
	Id: 1,
	Code: "ABC321",
	Name: "Television Sets",
}
</pre>

`http://localhost:4000/api/category/remove/{id}` - deletes a single category

`http://localhost:4000/api/category/list` - returns a list of categories


#### CustomerController.cs

`http://localhost:4000/api/customer/new` - creates a new customer

<pre>
{
	Name: "Customer Name",
	Surname: "Customer Surname",
	Email: "customer@gmail.com",
}
</pre>

`http://localhost:4000/api/category/{id}` - returns a single category by id

#### ProductController.cs

`http://localhost:4000/api/product/new` - creates a new product

<pre>
{
	Code: "PRO2123",
	Name: "Samsung 40IN LED TV",
	Price: 1500.00,
	CategoryId: "1",
}
</pre>

`http://localhost:4000/api/product/edit/{id}` - returns a single product by id


`http://localhost:4000/api/product/edit/{1}` - updated a single product

<pre>
{
	Id: 1,
	Code: "PRO2123",
	Name: "Samsung 40IN LED TV",
	Price: 1500.00,
	CategoryId: "1",
}
</pre>

`http://localhost:4000/api/product/remove/{1}` - deletes a single product by id

`http://localhost:4000/api/product/list` - returns a list of products

`http://localhost:4000/api/product/list-by-category/{categoryId}` - returns a list of products by category id

#### ShoppingBasketController.cs

`http://localhost:4000/api/shoppingbasket/new` - adds a new product to the shopping basket

<pre>
{
	CustomerId: 1,
	ProductId: 1,
	Price: 1500.00
}
</pre>

`http://localhost:4000/api/shoppingbasket/remove/{id}` - deletes a product from the shopping basket

`http://localhost:4000/api/shoppingbasket/basket-value/{customerId}` - returns the value of a customers basket


### Who do I talk to? ###

* email asheenk@gmail.com for further assistance

﻿
using App.Domain.Helpers;
using System.ComponentModel.DataAnnotations.Schema;

namespace App.Domain
{
    public class Product : BaseEntity
    {
        public string Code { get; set; }
        
        public string Name { get; set; }

        [Column(TypeName = "money")]
        public decimal Price { get; set; }
    }
}

﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace App.Domain.Helpers
{
    public class DataContext : DbContext
    {
        protected readonly IConfigurationRoot configuration;

        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
            configuration = new ConfigurationBuilder()
            .SetBasePath(System.AppDomain.CurrentDomain.BaseDirectory)
            .AddJsonFile("appsettings.json")
            .Build();
        }
        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            options.UseSqlServer(configuration.GetConnectionString("e-commerce"));
        }

        public DbSet<Customer> Customer { get; set; }

        public DbSet<Category> Category { get; set; }

        public DbSet<Category_Product> Category_Product { get; set; }

        public DbSet<Product> Product { get; set; }

        public DbSet<Bundle> Bundle { get; set; }

        public DbSet<ShoppingBasket> ShoppingBasket { get; set; }

    }
}

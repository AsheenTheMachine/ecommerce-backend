﻿using System.ComponentModel.DataAnnotations;
using System;

namespace App.Domain.Helpers
{
    public class BaseEntity : IBaseEntity
    {
        [Key]
        public long Id { get; set; }

        public long GetId()
        {
            return Id;
        }

    }
}

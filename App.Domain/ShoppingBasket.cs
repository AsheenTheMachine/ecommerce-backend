﻿
using System.ComponentModel.DataAnnotations.Schema;
using App.Domain.Helpers;

namespace App.Domain
{
    public class ShoppingBasket : BaseEntity
    {
        public long CustomerId { get; set; }
        public long ProductId { get; set; }
        
        [Column(TypeName = "money")]
        public decimal Price { get; set; }
        public string BundleCode{ get; set; }
    }
}

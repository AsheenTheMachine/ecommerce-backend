﻿
using App.Domain.Helpers;

namespace App.Domain
{
    public class Category : BaseEntity
    {
        public string Code { get; set; }
        
        public string Name { get; set; }

    }
}

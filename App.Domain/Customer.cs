﻿
using App.Domain.Helpers;
using System;

namespace App.Domain
{
    public class Customer : BaseEntity
    {
        public string Name { get; set; }
        
        public string Surname { get; set; }
        
        public string Email { get; set; }
    }
}

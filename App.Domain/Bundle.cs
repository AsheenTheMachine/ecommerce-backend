﻿
using App.Domain.Helpers;

namespace App.Domain
{
    public class Bundle : BaseEntity
    {
        public long ProductId { get; set; }
        
        public string BundleCode { get; set; }
        public int DiscountPercentage { get; set; }

    }
}

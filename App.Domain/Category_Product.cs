﻿
using App.Domain.Helpers;

namespace App.Domain
{
    public class Category_Product : BaseEntity
    {
        public long CategoryId { get; set; }
        
        public long ProductId { get; set; }

    }
}

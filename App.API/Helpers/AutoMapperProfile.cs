﻿using AutoMapper;
using App.Domain;
using App.Model;

namespace App.UI.Helpers
{
    public class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            #region Customer
            CreateMap<Customer, CustomerModel>().ReverseMap();
            CreateMap<Customer, CustomerModel>();
            #endregion

            #region Category
            CreateMap<Category, CategoryModel>().ReverseMap();
            CreateMap<Category, CategoryModel>();
            #endregion

            #region Product
            CreateMap<Product, ProductModel>().ReverseMap();
            CreateMap<Product, ProductModel>();
            #endregion

            #region Bundle
            CreateMap<Bundle, BundleModel>().ReverseMap();
            CreateMap<Bundle, BundleModel>();
            #endregion

            #region Shopping Basket
            CreateMap<ShoppingBasket, ShoppingBasketModel>().ReverseMap();
            CreateMap<ShoppingBasket, ShoppingBasketModel>();
            #endregion
        }
    }
}

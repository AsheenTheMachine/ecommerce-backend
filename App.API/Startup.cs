using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using App.Domain.Helpers;
using App.Repository.Interfaces;
using App.Repository.Helpers;
using App.Repository;
using System;
using Microsoft.OpenApi.Models;

namespace App
{
    public class Startup
    {
        private IConfiguration _configuration { get; }
        
        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<DataContext>();
            services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

            //swagger documentation of all end points
            services.AddSwaggerGen(c => {  
                    c.SwaggerDoc("v1", new OpenApiInfo {  
                        Version = "v1",  
                            Title = "e-Commerce API",  
                            Description = "Swagger api information for e-commerce backend",  
                            TermsOfService = new Uri("http://localhost:4000/api/terns"),
                            Contact = new OpenApiContact {  
                                Name = "Asheen Kamlal",
                                Email = "asheenk@gmail.com",  
                                Url = new Uri("https://www.linkedin.com/in/asheen-singh/"),
                            },  
                            License = new OpenApiLicense {  
                                Name = "Use under OpenApiLicense",  
                                Url = new Uri("http://localhost:4000/api/license"),
                            }  
                    });  
                });

            //not using the AllowAnyOrigin() as the combination of AllowAnyOrigin and AllowCredentials 
            //is considered an insecure CORS configuration
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy", builder => builder
                .WithOrigins("http://localhost:4200")
                .AllowAnyMethod()
                .AllowAnyHeader()
                .AllowCredentials());
            });

            services.AddControllers()
            .AddNewtonsoftJson(options =>
                options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore
            );

            //Dependency Injection
            services.AddScoped<ICustomerRepository, CustomerRepository>();
            services.AddScoped<ICategoryRepository, CategoryRepository>();
            services.AddScoped<IProductRepository, ProductRepository>();
            services.AddScoped<ICategory_ProductRepository, Category_ProductRepository>();
            services.AddScoped<IBundleRepository, BundleRepository>();
            services.AddScoped<IShoppingBasketRepository, ShoppingBasketRepository>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app)
        {
            app.UseRouting();

            // global cors policy
            app.UseCors("CorsPolicy");

            // generated swagger json and swagger ui middleware
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}

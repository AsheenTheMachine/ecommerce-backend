﻿using Microsoft.AspNetCore.Mvc;
using App.Repository.Helpers;
using App.Model;
using AutoMapper;
using App.Api.Helpers;
using Microsoft.Extensions.Options;
using System.Threading.Tasks;
using App.Domain;
using System.Collections.Generic;
using System.Linq;
using System;

namespace App.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ShoppingBasketController : BaseController
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly AppSettings _appSettings;

        public ShoppingBasketController(IUnitOfWork unitOfWork,
            IMapper mapper,
            IOptions<AppSettings> appSettings)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }


        [HttpGet]
        public IActionResult Index()
        {
            return Ok();
        }

        /// <summary>
        /// Adds a new item to shopping basket
        /// </summary>
        /// <returns></returns>
        [HttpPost("new")]
        public async Task<IActionResult> AddItem(ShoppingBasketModel model)
        {
            #region Validation
            if (!ModelState.IsValid)
                return BadRequest(new { message = GetErrors() });
            #endregion

            var basket = _mapper.Map<ShoppingBasket>(model);

            try
            {
                // add shopping basket object for inserting
                await _unitOfWork.ShoppingBasket.Add(basket);
                await _unitOfWork.Complete();

                //now lets go ahead and check for any bundle matches.

                //fetch distinct bundle codes by productId
                List<Bundle> bundleCodeList = _unitOfWork.Bundle.GetAllDistinctByProductId(model.ProductId).ToList();
                await _unitOfWork.Complete();

                List<BundleListModel> bundleListModelList = new List<BundleListModel>();

                //add to bundle list model
                foreach (var bundleCode in bundleCodeList)
                {
                    BundleListModel bundleListModel = new BundleListModel();
                    bundleListModel.BundleCode = bundleCode.BundleCode.ToString();

                    //for each bundle code, fetch the items in the bunlde
                    var bundledProducts = _unitOfWork.Bundle.GetAllByCode(bundleCode.BundleCode);
                    await _unitOfWork.Complete();

                    bundleListModel.BundleListItemsModel = new List<BundleModel>();

                    foreach (var bundleProduct in bundledProducts)
                    {
                        //add the bundled product to the bundle list, items list
                        var product = _mapper.Map<BundleModel>(bundleProduct);

                        bundleListModel.BundleListItemsModel.Add(product);
                    }

                    bundleListModelList.Add(bundleListModel);
                }

                if (bundleListModelList.Count > 0)
                {
                    //product item exists in 1 or more bundles. let's proceed to check if marked in basket

                    //fecth shopping basket to compare
                    //List<ShoppingBasket> basketItems = _unitOfWork.ShoppingBasket.GetBasketItemsByCustomerId(model.CustomerId);
                    List<ShoppingBasket> basketItems = _unitOfWork.ShoppingBasket.GetBasketItemsByCustomerId_NoBundle(model.CustomerId);
                    await _unitOfWork.Complete();

                    if (basketItems.Count() > 0)
                    {
                        //check if bundle list items exist in shopping basket
                        foreach (var bundleListItem in bundleListModelList)
                        {
                            //find all items from shopping basket that exusts in the bundled items list.
                            //only find items that have not already been marked as a bundle item in the basket

                            //filter the shopping basket to return disctinct products as there may be duplicates
                            List<ShoppingBasket> distinctBasketItems = basketItems
                                .Where(w => string.IsNullOrEmpty(w.BundleCode))
                                .GroupBy(p => p.ProductId)
                                .Select(g => g.First())
                                .ToList();

                            //filter the basket items that are in the bundle list
                            var basketFilter = from basketItem in distinctBasketItems
                                               join bundleItem in bundleListItem.BundleListItemsModel
                                               on basketItem.ProductId equals bundleItem.ProductId
                                               where basketItem.BundleCode == null || basketItem.BundleCode.Equals(string.Empty)
                                               select basketItem;

                            //if we have a match with the number of items in the bundle versus the filtered products in basket then proceed
                            //to update the basket item with the bundle code
                            if (basketFilter.Count() == bundleListItem.BundleListItemsModel.Count())
                            {
                                //we have a match of items in basket that are part of a bundle
                                //let's go ahead and mark the items as part of a bundle
                                foreach (var item in basketFilter.ToList())
                                {
                                    ShoppingBasket sb = item;
                                    sb.BundleCode = bundleListItem.BundleCode;

                                    //get the discount percentage from bundle
                                    int discount = bundleListItem.BundleListItemsModel
                                                    .Where(b => b.ProductId == item.ProductId)
                                                    .Select(x => x.DiscountPercentage)
                                                    .FirstOrDefault();

                                    sb.Price -= (discount * sb.Price) / 100;

                                    //add updated shopping basket entity for updating
                                    _unitOfWork.ShoppingBasket.Update(sb);
                                }
                            }
                        }
                    }
                }

                return Ok(await _unitOfWork.Complete());
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }

        /// <summary>
        /// Delete a shopping basket item
        /// </summary>
        /// <returns></returns>
        [HttpGet("remove/{id}")]
        public async Task<IActionResult> RemoveItem(long id)
        {
            #region Validation
            if (id <= 0)
                return BadRequest(new { message = "Id must be greater than 0" });
            #endregion

            try
            {
                var basket = await _unitOfWork.ShoppingBasket.Get(id);
                _unitOfWork.ShoppingBasket.Delete(basket);

                return Ok(await _unitOfWork.Complete());
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }

        /// <summary>
        /// Fetch the value of a customers basket
        /// </summary>
        /// <returns></returns>
        [HttpGet("basket-value/{customerId}")]
        public async Task<IActionResult> BasketValue(long customerId)
        {
            #region Validation
            if (customerId <= 0)
                return BadRequest(new { message = "Customer Id must be greater than 0" });
            #endregion

            try
            {
                var value = _unitOfWork.ShoppingBasket.GetBasketValueByCustomerId(customerId);
                await _unitOfWork.Complete();
                return Ok(value);
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }
    }
}

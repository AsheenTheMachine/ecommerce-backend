﻿using Microsoft.AspNetCore.Mvc;
using App.Repository.Helpers;
using App.Model;
using AutoMapper;
using App.Api.Helpers;
using Microsoft.Extensions.Options;
using System.Threading.Tasks;
using App.Domain;

namespace App.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomerController : BaseController
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly AppSettings _appSettings;

        public CustomerController(IUnitOfWork unitOfWork,
            IMapper mapper,
            IOptions<AppSettings> appSettings)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }


        [HttpGet]
        public IActionResult Index()
        {
            return Ok();
        }

        /// <summary>
        /// Adds a new customer
        /// </summary>
        /// <param name="CustomerModel"></param>
        /// <returns></returns>
        [HttpPost("new")]
        public async Task<IActionResult> CreateCustomer([FromBody] CustomerModel model)
        {
            #region Validation
            if (!ModelState.IsValid)
                return BadRequest(new { message = GetErrors() });
            
            #endregion

            var customer = _mapper.Map<Customer>(model);

            try
            {
                // add customer object for inserting
                await _unitOfWork.Customer.Add(customer);
                return Ok(await _unitOfWork.Complete());
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }

        /// <summary>
        /// Fetch customer by id
        /// </summary>
        /// <param name="id">id of customer object</param>
        /// <returns>CustomerModel</returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetCustomer(long id)
        {
            #region Validation

            if (id <= 0)
                return BadRequest(new { message = "Id must be greater than 0" });

            #endregion

            try
            {
                //fetch customer
                var customer = _unitOfWork.Customer.Get(id);
                await _unitOfWork.Complete();

                return Ok(customer);
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }

    }
}

﻿using System.Threading.Tasks;
using App.Api.Helpers;
using App.Model;
using App.Repository.Helpers;
using App.Repository.Interfaces;
using Microsoft.AspNetCore.Mvc;
using App.Domain;
using AutoMapper;

namespace App.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : BaseController
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly IProductRepository _productRepository;

        public ProductController(IUnitOfWork unitOfWork,
            IMapper mapper,
            IProductRepository productRepository)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;

            _productRepository = productRepository;
        }

 
        [HttpGet]
         public IActionResult Index()
        {
            return Ok();
        }

        /// <summary>
        /// Adds a new product
        /// </summary>
        /// <param name="ProductModel"></param>
        /// <returns></returns>
        [HttpPost("new")]
        public async Task<IActionResult> CreateProduct([FromBody] ProductModel model)
        {
            #region Validation
            if (!ModelState.IsValid)
                return BadRequest(new { message = GetErrors() });
            #endregion

            var product = _mapper.Map<Product>(model);

            try
            {
                // add product object for inserting
                await _unitOfWork.Product.Add(product);

                //let's commit the product 1st, to receive a new product Id
                await _unitOfWork.Complete();

                //add new product to category
                Category_Product cp = new ()
                {
                    ProductId = product.Id,
                    CategoryId = model.CategoryId
                };

                // add category_product object for inserting
                await _unitOfWork.Category_Product.Add(cp);

                return Ok(await _unitOfWork.Complete());
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }
        
        /// <summary>
        /// Fetch product for edit
        /// </summary>
        /// <param name="id">id of product object</param>
        /// <returns>ProductModel</returns>
        [HttpGet("edit/{id}")]
        public async Task<IActionResult> GetProduct(long id)
        {
            #region Validation
            if (id <= 0)
                return BadRequest(new { message = "Id must be greater than 0" });
            #endregion

            try
            {
                //fetch product
                var product = _unitOfWork.Product.Get(id);
                await _unitOfWork.Complete();

                return Ok(product);
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }


        /// <summary>
        /// Edit a product
        /// </summary>
        /// <param name="id">id of the object to update</param>
        /// <param name="ProductModel">model of data to update</param>
        /// <returns></returns>
        [HttpPost("edit/{id}")]
        public async Task<IActionResult> UpdateProduct([FromBody] ProductModel model, long id)
        {
            #region Validation

            if (!ModelState.IsValid)
                return BadRequest(new { message = GetErrors() });

            
            if (id <= 0)
                return BadRequest(new { message = "Id must be greater than 0" });

            #endregion

            try
            {
                 var product = _mapper.Map<Product>(model);
                 if(product.Id == 0)
                    product.Id = id;

                // add product object for updating
                _unitOfWork.Product.Update(product);
                return Ok(await _unitOfWork.Complete());
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }


        /// <summary>
        /// Delete a Product
        /// </summary>
        /// <param name="ProductModel"></param>
        /// <returns></returns>
        [HttpGet("remove/{id}")]
        public async Task<IActionResult> RemoveProduct(long id)
        {
            #region Validation
            if (id <= 0)
                return BadRequest(new { message = "Id must be greater than 0" });
            #endregion

            try
            {
                var product = await _unitOfWork.Product.Get(id);
                _unitOfWork.Product.Delete(product);

                return Ok(await _unitOfWork.Complete());
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }
    
        /// <summary>
        /// Fetch Product List
        /// </summary>
        /// <returns>List of products</returns>
        [HttpGet("list")]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                var productList =  _unitOfWork.Product.GetAll();
                await _unitOfWork.Complete();

                return Ok(productList);
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }


        /// <summary>
        /// Fetch Product List BY categoryId
        /// </summary>
        /// <returns>List of products BY category id</returns>
        [HttpGet("list-by-category/{categoryId}")]
        public async Task<IActionResult> GetAllByCategory(long categoryId)
        {
            #region Validation
            if (categoryId <= 0)
                return BadRequest(new { message = "categoryId must be greater than 0" });
            #endregion

            try
            {
                var productList =  _unitOfWork.Product.GetAllByCategoryId(categoryId);
                await _unitOfWork.Complete();

                return Ok(productList);
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }

    }
}

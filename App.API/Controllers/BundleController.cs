﻿using Microsoft.AspNetCore.Mvc;
using App.Repository.Helpers;
using App.Model;
using AutoMapper;
using App.Api.Helpers;
using Microsoft.Extensions.Options;
using System.Threading.Tasks;
using App.Domain;

namespace App.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BundleController : BaseController
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly AppSettings _appSettings;

        public BundleController(IUnitOfWork unitOfWork,
            IMapper mapper,
            IOptions<AppSettings> appSettings)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return Ok();
        }

        /// <summary>
        /// Adds a new item to bundle
        /// </summary>
        /// <param name="BundleModel"></param>
        /// <returns></returns>
        [HttpPost("new")]
        public async Task<IActionResult> CreateBundleItem([FromBody] BundleModel model)
        {
            #region Validation
            if (!ModelState.IsValid)
                return BadRequest(new { message = GetErrors() });
            
            #endregion

            var bundle = _mapper.Map<Bundle>(model);

            try
            {
                // add bundle object for inserting
                await _unitOfWork.Bundle.Add(bundle);
                return Ok(await _unitOfWork.Complete());
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }

        /// <summary>
        /// Fetch bundle list by bundle code
        /// </summary>
        /// <param name="code">code of bundle</param>
        /// <returns>List of Bundle Items</returns>
        [HttpGet("list-by-code/{code}")]
        public async Task<IActionResult> GetBundleItems(string code)
        {
            #region Validation

            if (string.IsNullOrEmpty(code))
                return BadRequest(new { message = "Bundle Code is required!" });

            #endregion

            try
            {
                //fetch bundle items
                var bundleList = _unitOfWork.Bundle.GetAllByCode(code);
                await _unitOfWork.Complete();

                return Ok(bundleList);
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }


        /// <summary>
        /// Fetch all bundles
        /// </summary>
        /// <returns>List of Bundles</returns>
        [HttpGet("list")]
        public async Task<IActionResult> GetAllBundles()
        {
            try
            {
                //fetch all bundles
                var bundleList = _unitOfWork.Bundle.GetAllDistinct();
                await _unitOfWork.Complete();

                return Ok(bundleList);
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }

        /// <summary>
        /// Fetch bundle list by bundle product id
        /// </summary>
        /// <param name="productId">product id to search by</param>
        /// <returns>List of Bundle Items</returns>
        [HttpGet("list-by-productId/{productId}")]
        public async Task<IActionResult> GetBundleItemsByProductId(long productId)
        {
            #region Validation
            if (productId == 0)
                return BadRequest(new { message = "productId is required!" });

            #endregion

            try
            {
                //fetch bundle items
                var bundleList = _unitOfWork.Bundle.GetAllDistinctByProductId(productId);
                await _unitOfWork.Complete();

                return Ok(bundleList);
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }

    }
}

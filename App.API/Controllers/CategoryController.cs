﻿using System.Threading.Tasks;
using App.Api.Helpers;
using App.Model;
using App.Repository.Helpers;
using App.Repository.Interfaces;
using Microsoft.AspNetCore.Mvc;
using App.Domain;
using AutoMapper;

namespace App.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CategoryController : BaseController
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ICategoryRepository _categoryRepository;

        public CategoryController(IUnitOfWork unitOfWork,
            IMapper mapper,
            ICategoryRepository categoryRepository)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;

            _categoryRepository = categoryRepository;
        }

 
        [HttpGet]
         public IActionResult Index()
        {
            return Ok();
        }

        /// <summary>
        /// Adds a new category
        /// </summary>
        /// <param name="CategoryModel"></param>
        /// <returns></returns>
        [HttpPost("new")]
        public async Task<IActionResult> CreateCategory([FromBody] CategoryModel model)
        {
            #region Validation
            if (!ModelState.IsValid)
                return BadRequest(new { message = GetErrors() });
            #endregion

            var category = _mapper.Map<Category>(model);

            try
            {
                // add category object for inserting
                await _unitOfWork.Category.Add(category);
                int complete = await _unitOfWork.Complete();

                return Ok(complete);
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }
        
        /// <summary>
        /// Fetch category for edit, by id
        /// </summary>
        /// <param name="id">id of category object</param>
        /// <returns>CategoryModel</returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetCategoryById(long id)
        {
            #region Validation
            if (id <= 0)
                return BadRequest(new { message = "Id must be greater than 0!" });
            #endregion

            try
            {
                //fetch category by id
                var category = await _unitOfWork.Category.Get(id);
                await _unitOfWork.Complete();

                if(category == null)
                    return BadRequest(new { message = "Category does not exist!" });

                return Ok(category);
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }


        /// <summary>
        /// Edit a category
        /// </summary>
        /// <param name="id">id of the object to update</param>
        /// <param name="CategoryModel">model of data to update</param>
        /// <returns></returns>
        [HttpPut("edit/{id}")]
        public async Task<IActionResult> UpdateCategory([FromBody] CategoryModel model, long id)
        {
            #region Validation
            if (!ModelState.IsValid)
                return BadRequest(new { message = GetErrors() });
            
            if (id <= 0)
                return BadRequest(new { message = "Id must be greater than 0" });
            #endregion

            try
            {
                 var category = _mapper.Map<Category>(model);
                 if(category.Id == 0)
                    category.Id = id;

                // add category object for updating
                _unitOfWork.Category.Update(category);
                return Ok(await _unitOfWork.Complete());
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }


        /// <summary>
        /// Delete a category
        /// </summary>
        /// <param name="CategoryModel"></param>
        /// <returns></returns>
        [HttpGet("remove/{id}")]
        public async Task<IActionResult> RemoveCategory(long id)
        {
            #region Validation
            if (id <= 0)
                return BadRequest(new { message = "Id must be greater than 0" });
            #endregion

            try
            {
                var category = await _unitOfWork.Category.Get(id);
                _unitOfWork.Category.Delete(category);

                return Ok(await _unitOfWork.Complete());
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }
    
        /// <summary>
        /// Fetch category list
        /// </summary>
        /// <returns>A list of categories</returns>
        [HttpGet("list")]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                var categoryList =  await _unitOfWork.Category.GetAll();
                await _unitOfWork.Complete();

                return Ok(categoryList);
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }

    }
}

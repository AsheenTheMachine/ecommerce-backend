﻿using App.Domain;
using App.Repository.Helpers;
using App.Domain.Helpers;
using App.Repository.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System;

namespace App.Repository
{
    public class BundleRepository : GenericRepository<Bundle>, IBundleRepository
    {
        public BundleRepository(DataContext context) : base(context) {}      

        //fetch list of bundled items by bundle code
        public List<Bundle> GetAllByCode(string code)
        {
            var bundleItems = _context.Bundle.Where(b => b.BundleCode.Trim() == code.Trim()).ToList();
            return bundleItems.ToList();
        } 

        
        //fetch list of bundles
        public IEnumerable<object> GetAllDistinct()
        {
            var bundles = _context.Bundle
                .ToList()
                .GroupBy(x => x.BundleCode)
                .Select(x => x.First())
                .Select(
                    b => new
                    {
                        b.BundleCode,
                    }
                );

            return bundles;
        }

        //fetch list of bundles by product Id
        public List<Bundle> GetAllDistinctByProductId(long productId)
        {
            var bundles = _context.Bundle
                .Where(b => b.ProductId == productId).ToList()
                .GroupBy(x => x.BundleCode)
                .Select(x => x.First())
                .Select(
                    b => new Bundle
                    {
                        BundleCode = b.BundleCode,
                        Id = b.Id,
                        ProductId = b.ProductId
                    }
                );

            return bundles.ToList();
        } 
    }
}

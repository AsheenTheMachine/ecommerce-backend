﻿using System.Collections.Generic;
using App.Domain;
using App.Repository.Helpers;

namespace App.Repository.Interfaces
{

    public interface IShoppingBasketRepository : IGenericRepository<ShoppingBasket>
    {
        decimal GetBasketValueByCustomerId(long customerId);
        List<ShoppingBasket> GetBasketItemsByCustomerId(long customerId);
        List<ShoppingBasket> GetBasketItemsByCustomerId_NoBundle(long customerId);
    }
}

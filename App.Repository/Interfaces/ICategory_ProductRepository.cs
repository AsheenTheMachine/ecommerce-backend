﻿using App.Domain;
using App.Repository.Helpers;

namespace App.Repository.Interfaces
{

    public interface ICategory_ProductRepository : IGenericRepository<Category_Product>
    {
        
    }
}

﻿using System.Collections.Generic;
using App.Domain;
using App.Repository.Helpers;

namespace App.Repository.Interfaces
{

    public interface IBundleRepository : IGenericRepository<Bundle>
    {

        List<Bundle> GetAllByCode(string code);
        IEnumerable<object> GetAllDistinct();
        List<Bundle> GetAllDistinctByProductId(long productId);
    }
}

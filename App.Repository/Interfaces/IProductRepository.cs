﻿using App.Domain;
using App.Repository.Helpers;
using System.Collections.Generic;

namespace App.Repository.Interfaces
{

    public interface IProductRepository : IGenericRepository<Product>
    {
        List<Product> GetAllByCategoryId(long categoryId);
    }
}

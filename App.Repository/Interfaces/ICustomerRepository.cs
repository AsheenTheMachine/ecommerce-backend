﻿using App.Domain;
using App.Repository.Helpers;

namespace App.Repository.Interfaces
{

    public interface ICustomerRepository : IGenericRepository<Customer>
    {
        Customer GetByEmail(string email);
    }
}

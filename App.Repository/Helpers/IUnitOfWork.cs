﻿using System;
using System.Threading.Tasks;
using App.Repository.Interfaces;

namespace App.Repository.Helpers
{
    public interface IUnitOfWork 
    {

        ICustomerRepository Customer { get; }
        ICategoryRepository Category { get; }
        IProductRepository Product { get; }
        ICategory_ProductRepository Category_Product { get; }
        IBundleRepository Bundle { get; }
        IShoppingBasketRepository ShoppingBasket { get; }

        //IProductRepository Product { get; }

        Task<int> Complete();
    }
}

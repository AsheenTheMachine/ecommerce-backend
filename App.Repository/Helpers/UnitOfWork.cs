﻿using System;
using App.Repository.Interfaces;
using App.Domain.Helpers;
using System.Threading.Tasks;

namespace App.Repository.Helpers
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly DataContext _context;

        public ICustomerRepository Customer { get; }
        public ICategoryRepository Category { get; }
        public IProductRepository Product { get; }
        public ICategory_ProductRepository Category_Product { get; }
        public IShoppingBasketRepository ShoppingBasket { get; }
        public IBundleRepository Bundle { get; }

        public UnitOfWork(DataContext dataContext, 
            ICustomerRepository customerRepository,
            ICategoryRepository categoryRepository,
            IProductRepository productRepository,
            ICategory_ProductRepository category_ProductRepository,
            IBundleRepository bundleRepository,
            IShoppingBasketRepository shoppingBasketRepository)
        {
            this._context = dataContext;

            this.Customer = customerRepository;
            this.Category = categoryRepository;
            this.Product = productRepository;
            this.Category_Product = category_ProductRepository;
            this.Bundle = bundleRepository;
            this.ShoppingBasket = shoppingBasketRepository;
        }

        public async Task<int> Complete()
        {
            return await _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
        }
    }
}

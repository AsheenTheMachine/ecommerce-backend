﻿using App.Domain;
using App.Repository.Helpers;
using App.Domain.Helpers;
using App.Repository.Interfaces;
namespace App.Repository
{
    public class Category_ProductRepository : GenericRepository<Category_Product>, ICategory_ProductRepository
    {
        public Category_ProductRepository(DataContext context) : base(context) {}       
    }
}

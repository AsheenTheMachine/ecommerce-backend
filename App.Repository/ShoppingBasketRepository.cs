﻿using App.Domain;
using App.Repository.Helpers;
using App.Domain.Helpers;
using App.Repository.Interfaces;
using System.Linq;
using System.Collections.Generic;

namespace App.Repository
{
    public class ShoppingBasketRepository : GenericRepository<ShoppingBasket>, IShoppingBasketRepository
    {
        public ShoppingBasketRepository(DataContext context) : base(context) {}

        //get totale value of basket by customer id
        public decimal GetBasketValueByCustomerId(long customerId)
        {
            return _context.ShoppingBasket
                    .Where(s => s.CustomerId == customerId)
                    .Sum(s => s.Price);
        }

        //get all items in basket by customer id
        public List<ShoppingBasket> GetBasketItemsByCustomerId(long customerId)
        {
            return _context.ShoppingBasket
                    .Where(s => s.CustomerId == customerId)
                    .ToList();
        }

        //get all items in basket by customer id that don't have a bundle applied
        public List<ShoppingBasket> GetBasketItemsByCustomerId_NoBundle(long customerId)
        {
            return _context.ShoppingBasket
                    .Where(s => (s.CustomerId == customerId) && string.IsNullOrEmpty(s.BundleCode))
                    .ToList();
        }
    }
}

﻿using System.Linq;
using App.Domain;
using App.Repository.Helpers;
using App.Domain.Helpers;
using App.Repository.Interfaces;

namespace App.Repository
{
    public class CustomerRepository : GenericRepository<Customer>, ICustomerRepository
    {
        public CustomerRepository(DataContext context) : base(context) {}

        public Customer GetByEmail(string email)
        {
            return _context.Customer
                            .Where(e => e.Email == email)
                            .FirstOrDefault();
        }
    }
}

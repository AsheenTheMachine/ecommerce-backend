﻿using System.Linq;
using App.Domain;
using App.Repository.Helpers;
using App.Domain.Helpers;
using App.Repository.Interfaces;
using System.Collections.Generic;

namespace App.Repository
{
    public class ProductRepository : GenericRepository<Product>, IProductRepository
    {
        public ProductRepository(DataContext context) : base(context) {}

        //join the category_product table with product table, by productId
        public List<Product> GetAllByCategoryId(long categoryId)
        {
            var products = _context.Product
                .Join(_context.Category_Product, p => p.Id, cp => cp.ProductId, (p, cp) => new { p, cp })
                .Select(prods => new Product{ 
                    Id = prods.p.Id,
                    Code = prods.p.Code,
                    Name = prods.p.Name,
                    Price = prods.p.Price,
                }).ToList();

            return products;
        }
       
    }
}

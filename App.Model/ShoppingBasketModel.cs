﻿using App.Model.Helpers;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace App.Model
{
    public class ShoppingBasketModel : BaseModel
    {
        
        [Required(ErrorMessage = "Customer is required!")]
        public long CustomerId { get; set; }
        
        [Required(ErrorMessage = "Product is required!")]
        public long ProductId { get; set; }

        [Required(ErrorMessage = "Price is required!")]
        [Column(TypeName = "money")]
        public decimal Price { get; set; }
        public string BundleCode{ get; set; }
    }
}

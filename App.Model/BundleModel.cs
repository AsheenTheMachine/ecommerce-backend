﻿using App.Model.Helpers;
using System.ComponentModel.DataAnnotations;

namespace App.Model
{
    public class BundleModel : BaseModel
    {
        [Required(ErrorMessage = "Product is required!")]
        public long ProductId { get; set; }

        [Required(ErrorMessage = "Bundle Code is required!")]
        [StringLength(10, ErrorMessage = "Bundle Code cannot be longer than 10 characters!")]
        public string BundleCode { get; set; }

        [Required(ErrorMessage = "Discount Percentage is required!")]
        [Range(1, 100, ErrorMessage = "Discount must be between 0 and 101")]
        public int DiscountPercentage { get; set; }
    }
}

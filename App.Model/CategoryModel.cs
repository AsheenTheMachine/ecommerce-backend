﻿using App.Model.Helpers;
using System.ComponentModel.DataAnnotations;

namespace App.Model
{
    public class CategoryModel : BaseModel
    {
        [Required(ErrorMessage = "Category Code is required!")]
        [StringLength(10, ErrorMessage = "Category Code cannot be longer than 10 characters!")]
        public string Code { get; set; }
        
        [Required(ErrorMessage = "Category Name is required!")]
        [StringLength(50, ErrorMessage = "Category Name cannot be longer than 200 charaters!")]
        public string Name { get; set; }
    }
}

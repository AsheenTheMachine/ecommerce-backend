﻿using App.Model.Helpers;
using System.ComponentModel.DataAnnotations;

namespace App.Model
{
    public class ProductModel : BaseModel
    {
        [Required(ErrorMessage = "Product Code is required!")]
        [StringLength(10, ErrorMessage = "Product Code cannot be longer than 10 characters!")]
        public string Code { get; set; }
        
        [Required(ErrorMessage = "Product Name is required!")]
        [StringLength(50, ErrorMessage = "Product Name cannot be longer than 200 characters!")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Price is required!")]
        public decimal Price { get; set; }

        [Required(ErrorMessage = "Category is required!")]
        public long CategoryId { get; set; }

    }
}

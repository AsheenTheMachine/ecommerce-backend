﻿namespace App.Model.Helpers
{
    public interface IBaseModel
    {
        long GetId();
    }
}

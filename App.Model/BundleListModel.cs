﻿using System.Collections.Generic;
using App.Model.Helpers;

namespace App.Model
{
    public class BundleListModel : BaseModel
    {
        public string BundleCode { get; set; }

        public List<BundleModel> BundleListItemsModel { get; set; }
    }
}

using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using App.Model;
using App.Test.Fixtures;
using App.Test.Helpers;
using FluentAssertions;
using Xunit;
using Newtonsoft.Json;
using App.Domain;
using System;

namespace App.Test
{
    public class CategoryControllerTest : IntegrationTest
    {
        protected readonly string baseUrl = "api/category/";

        public CategoryControllerTest(ApiWebApplicationFactory fixture)
            : base(fixture) { }


        [Fact]
        public async Task POST_creates_a_new_category()
        {
            int number = RandomNumber();

            CategoryModel cm = new CategoryModel();
            cm.Code = "CAT1" + number;
            cm.Name = "New Category " + number;

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, baseUrl + "new");
            request.Content = new StringContent(JsonConvert.SerializeObject(cm),
                                                Encoding.UTF8, 
                                                "application/json");

            var category = await _client.SendAsync(request);

            //check is respond status code is OK
            category.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [Fact]
        public async Task GET_retrieves_list_of_categories()
        {
            var list = await _client.GetAndDeserialize<Category[]>(baseUrl + "list");

            //check if the number of records are more that 0, therefore records exist
            list.Should().HaveCountGreaterThan(0);
            
            //loop through the items and check if each Id is greater than 0
            foreach (var item in list)
            {
                item.Id.Should().BeGreaterThan(0);
            }
        }

        [Fact]
        public async Task GET_retrieves_single_category_by_id()
        {
            var response = await _client.GetAsync(baseUrl + "1");
            var responseContent = await response.Content.ReadAsStringAsync();
            var category = JsonConvert.DeserializeObject<Category>(responseContent);

            //check is respond status code is OK
            response.StatusCode.Should().Be(HttpStatusCode.OK);

            //check if Id = 1
            category.Id.Should().Be(1);
        }
    }
}

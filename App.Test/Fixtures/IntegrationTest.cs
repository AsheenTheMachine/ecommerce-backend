﻿using Microsoft.Extensions.Configuration;
using Respawn;
using System;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using Xunit;

namespace App.Test.Fixtures
{
    [Trait("Category", "Integration")]
    public abstract class IntegrationTest : IClassFixture<ApiWebApplicationFactory>
    {
        protected readonly ApiWebApplicationFactory _factory;
        protected readonly HttpClient _client;

        public IntegrationTest(ApiWebApplicationFactory fixture)
        {
            _factory = fixture;
            _client = _factory.CreateClient();

            _client.DefaultRequestHeaders
                    .Accept
                    .Add(
                        new MediaTypeWithQualityHeaderValue("application/json")
                        {
                            CharSet = Encoding.UTF8.WebName
                        });
        }

        public int RandomNumber()
        {
            Random rnd = new Random();
            return rnd.Next(1000);
        }
    }
}

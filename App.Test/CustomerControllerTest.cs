using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using App.Model;
using App.Test.Fixtures;
using App.Test.Helpers;
using FluentAssertions;
using Xunit;
using Newtonsoft.Json;
using App.Domain;
using System;

namespace App.Test
{
    public class CustomerControllerTest : IntegrationTest
    {
        protected readonly string baseUrl = "api/customer/";

        public CustomerControllerTest(ApiWebApplicationFactory fixture)
            : base(fixture) { }


        [Fact]
        public async Task POST_creates_a_new_customer()
        {
            int number = RandomNumber();

            CustomerModel cm = new CustomerModel();
            cm.Name = "Name " + number;
            cm.Surname = "Surname " + number;
            cm.Email = number + "@gmail.com";

            HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Post, baseUrl + "new");
            request.Content = new StringContent(JsonConvert.SerializeObject(cm),
                                                Encoding.UTF8, 
                                                "application/json");

            var  customer = await _client.SendAsync(request);

            //check if respond status code is OK
            customer.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [Fact]
        public async Task GET_retrieves_single_customer_by_id()
        {
            var response = await _client.GetAsync(baseUrl + "1");
            var responseContent = await response.Content.ReadAsStringAsync();
            var customer = JsonConvert.DeserializeObject<Category>(responseContent);

            //check is respond status code is OK
            response.StatusCode.Should().Be(HttpStatusCode.OK);

            //check if Id = 1
            customer.Id.Should().Be(1);
        }
    }
}
